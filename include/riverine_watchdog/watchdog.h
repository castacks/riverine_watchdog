
#ifndef _WATCHDOG_H_
#define _WATCHDOG_H_

/**\file  watchdog.h
  * \brief   Implements the interface for each node in the watchdog.
  * Create an instance of this class and call setup to initialize the class
  * After initializing pet the watchdog with "alive()" to keep it from expiring.
  * If a fault should occur actively send "fault". For out of normal non-critical behavior
  * issue "warn()".
  * \author  Sebastian Scherer <basti@cmu.edu>
  *
  */

#include <ros/ros.h>
#include <ros/node_handle.h>

#include <riverine_watchdog/WatchdogStatus.h>

namespace CA
{

  /**
   * \brief Class that pets the watchdog to ensure everything is ok.
   */
  class PetWatchdog
  {
  protected:
    ros::Time last_;
    ros::Time lastActual_;
    ros::Duration updateDelay_;

    bool setup_;
    riverine_watchdog::WatchdogStatus status;
    ros::Publisher petPub;
    void sendMessage(int status);
    double callFrequency(double duration); 
 public:
    PetWatchdog();
    bool setup(ros::NodeHandle &n);
    void alive();
    void warn();
    void fault();
    void init();
 
  };


};

#endif
