#ifndef _SENSORS_H_
#define _SENSORS_H_

#include <string>
#include <riverine_watchdog/WatchedElement.h>
#include <riverine_watchdog/WatchdogStatus.h>
#include <riverine_watchdog/HealthStatus.h>
#include <riverine_watchdog/WatchdogCommand.h>
#include <ros/node_handle.h>
#include <ros/ros.h>
#include <vector>
#include <map>


namespace CA
{
  class WatchdogProcesser
  {
  protected:
    double initialDelay;
    bool setup_;
    bool armed_;
    int level;
    std::map < std::string, riverine_watchdog::WatchedElement> elements;
    ros::NodeHandle &n;
    ros::Subscriber sub;
    ros::Subscriber subCommand;
    ros::Publisher  statusPub;
    riverine_watchdog::HealthStatus healthStatus;
    ros::Timer timer;
    ros::Time startTime;
    void processWatchdog(const riverine_watchdog::WatchdogStatus::ConstPtr &msg);
    void processCommand(const riverine_watchdog::WatchdogCommand::ConstPtr &msg);
    void processTimer(const ros::TimerEvent &ev);
  public:
    WatchdogProcesser(ros::NodeHandle &n);
    bool setup();
    void reset();

    
  };

}


#endif
