#ifndef _PRC_INFORMATION_
#define _PRC_INFORMATION_

#include <riverine_watchdog/ProcessInformation.h>

namespace CA
{
  namespace PRCInformation
  {
    //If -1 or no argument is given the function will return it's own statistics.
    bool getStatistics(riverine_watchdog::ProcessInformation &msg, int pid=-1);
    
  };


}


#endif
