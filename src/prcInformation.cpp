/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include <riverine_watchdog/prcInformation.h>
#include <ros/ros.h>
#include <unistd.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

using namespace std;



bool CA::PRCInformation::getStatistics(riverine_watchdog::ProcessInformation &msg, int pid)
{
  using std::ios_base;
  using std::ifstream;
  //Open the 
  stringstream out;
  if(pid == -1)
    out<<"/proc/self/stat";
  else
    out<<"/proc/" << pid << "/stat";
  std::string filename = out.str(); 

  ifstream stat_stream(filename.c_str(),ios_base::in);
  if(!stat_stream)
    return false;
  //We have a good file. Now let's read the variables:
  msg.collected = ros::Time::now();   
  //Values we don't care about
  string tty_nr, tpgid, itrealvalue,signal, blocked, sigignore, sigcatch,wchan, nswap, cnswap, exit_signal;
  //Read in the process information
  stat_stream >> msg.pid >> msg.tcomm >> msg.state >> msg.ppid >> msg.pgrp >> msg.sid >> tty_nr
	      >> tpgid >> msg.flags >> msg.min_flt >> msg.cmin_flt >> msg.maj_flt >> msg.cmaj_flt
	      >> msg.utime >> msg.stime >> msg.cutime >> msg.cstime >> msg.priority >> msg.nice
	      >> msg.num_threads >> itrealvalue >> msg.start_time >> msg.vsize >> msg.rss 
	      >> msg.rsslim >> msg.esp >> msg.eip >> signal >> blocked >> sigignore >> sigcatch
	      >> wchan >> nswap >> cnswap >> exit_signal >> msg.task_cpu >> msg.rt_priority
	      >> msg.policy >> msg.blkio_ticks >> msg.gtime >> msg.cgtime; 
  stat_stream.close();
  msg.pagesize = sysconf(_SC_PAGE_SIZE);
  return true;

}
