#!/usr/bin/env python

'''
'''

import math
import string
import datetime
import threading

from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer

import rospy
import roslib 

import rosgraph
import rosgraph.names
import rosnode
import rostopic

from watchdog.msg import WatchdogInfo
from watchdog.msg import WDNodeInfo
from watchdog.msg import WDTopicInfo
from watchdog.msg import WDTopicFieldInfo


#==============================================================================
# Constants

WATCHDOG_SCAN_RATE_HZ = 1

MAX_NUM_MONITORED_NODES = 30

MAX_NUM_MONITORED_TOPICS = 2*MAX_NUM_MONITORED_NODES

MAX_NUM_MONITORED_FIELDS = 2*MAX_NUM_MONITORED_TOPICS


HEART_BEAT_TIMEOUT_WARN  = 3.0
HEART_BEAT_TIMEOUT_ERROR = 6.0


#==============================================================================
# Global variables

'''
List of MonitoredNode objects
'''
global g_objlist_nodes

'''
List of MonitoredTopic objects
'''
global g_objlist_topics

'''
List of MonitoredTopicField objects
'''
global g_objlist_fields


'''
List of  objects NodeHealthState
'''
global g_objlist_health


'''
Watchdog information publisher
'''
global g_pub_watchdog_info


'''
GLOBAL VAR
ROSTopicHz list : list of topics that are monitored.
This list is used by topic monitoring and topic field monitoring.
If the same topic is used in more that one place, it is shared through this list.
'''
global g_objlist_rostopichz


#==============================================================================

'''
This class stores:
- the times the messages were received (last window_size messages are stored)
- the last message received from a specific topic
It also provide the message reception rate (calculated from the stored times).
'''
class ROSTopicHz(object):
    """
    ROSTopicHz receives messages for a topic and computes frequency stats
    """
    def __init__(self, topic_name, window_size):
        import threading
        self.lock = threading.Lock()
        self.last_printed_tn = 0
        self.sub = None
        self.msg = None
        self.msg_t0 = -1.
        self.msg_tn = 0
        self.times =[]
        
        self.topic_name = topic_name
        # can't have infinite window size due to memory restrictions
        if window_size < 0:
            window_size = 50000
        self.window_size = window_size
        self.mean = 0.0
        self.std_dev = 0.0
                
    def callback_hz(self, m, topic):
        """
        ros sub callback
        :param m: Message instance
        """
        #rospy.logerr(topic)
        self.msg = m
        with self.lock:
            curr_rostime = rospy.get_rostime()

            # time reset
            if curr_rostime.is_zero():
                if len(self.times) > 0:
                    rospy.logerr("time has reset, resetting counters")
                    self.times = []
                return
            
            curr = curr_rostime.to_sec()
            if self.msg_t0 < 0 or self.msg_t0 > curr:
                self.msg_t0 = curr
                self.msg_tn = curr
                self.times = []
            else:
                self.times.append(curr - self.msg_tn)
                self.msg_tn = curr

            #only keep statistics for the last 10000 messages so as not to run out of memory
            if len(self.times) > self.window_size - 1:
                self.times.pop(0)

    def get_hz(self):
        """
        get the average publishing rate
        """
        if not self.times:
            return -2
        elif self.msg_tn == self.last_printed_tn:
            print("no new messages")
            return -1
        with self.lock:
            #frequency
            
            # kwc: In the past, the rate decayed when a publisher
            # dies.  Now, we use the last received message to perform
            # the calculation.  This change was made because we now
            # report a count and keep track of last_printed_tn.  This
            # makes it easier for users to see when a publisher dies,
            # so the decay is no longer necessary.
            
            n = len(self.times)
            #rate = (n - 1) / (rospy.get_time() - self.msg_t0)
            mean = sum(self.times) / n
            rate = 1./mean if mean > 0. else 0

            #std dev
            std_dev = math.sqrt(sum((x - mean)**2 for x in self.times) /n)

            # min and max
            max_delta = max(self.times)
            min_delta = min(self.times)

            self.last_printed_tn = self.msg_tn
            self.mean = mean
            self.std_dev = std_dev
            
            #rospy.logdebug("average rate: %.3f\n\tmin: %.3fs max: %.3fs std dev: %.5fs window: %s"%(rate, min_delta, max_delta, std_dev, n+1))
            return rate


    def get_msg(self):
        #rospy.logerr(self.msg)
        return [self.msg, self.msg_tn]



#==============================================================================


'''
Check if the topic is available in ROSTopicHz list.
If not, new object is created and stored.
'''
def getROSTopicHz(topic_name):
    rospy.logerr("STuff ");
    global g_objlist_rostopichz
    for auxrostopic in g_objlist_rostopichz:
        if (auxrostopic.topic_name == topic_name):
            rospy.loginfo("Topic exists : " + topic_name);
            return auxrostopic

    #check if topic is being published
    master = rosgraph.Master('/rostopic')
    state = master.getSystemState()
    pubs, subs, aux = state
    #rospy.logerr(pubs)
    found = False
    for pub in pubs:
        if (pub[0].find(topic_name) != -1):
            found = True

    #topic not available
    if (found == False):
        rospy.logerr("ERROR: Topic not available : " + topic_name);
        return None

    msg_class, real_topic, msg_eval = rostopic.get_topic_class(topic_name, blocking=True)

    #topic is available - create object to monitor
    aux = ROSTopicHz(real_topic, 100)
    aux.sub = rospy.Subscriber(real_topic, msg_class, aux.callback_hz, topic_name) 
    g_objlist_rostopichz.append(aux)
    #rospy.loginfo("Topic available : " + real_topic);
    return aux



#==============================================================================
# Class definitions
#==============================================================================

'''
Base class for items that are monitored
'''
class MonitoredItem:
    stateOK    = 0 
    stateWarn  = 1
    stateErr   = 2
    stateUndef = 255
   
    def __init__(self, name):
        self.init(name)

    def init(self, name):
        self.name = name
        self.category = 'Item'
        self.curr_state = self.stateUndef 
        self.prev_state = self.stateUndef
        self.state_change_description = ''

    def setState(self, state, description):
        if ((state != self.curr_state) or (self.state_change_description != description)):
            self.prev_state = self.curr_state 
            self.curr_state = state
            self.state_change_description = description
            self.print_state()

    def get_state_str(self):
        if (self.curr_state == self.stateOK): 
            return "OK"
        elif (self.curr_state == self.stateErr): 
            return "ERROR"
        elif (self.curr_state == self.stateWarn): 
            return "WARNING"
        else:
            return "UNDEFINED"

    def print_state(self):
        msg_str = self.category + ":" + self.name
        msg_str = msg_str + " (state = {0})".format(self.curr_state)
        msg_str = msg_str + " - " + self.state_change_description
        rospy.loginfo(msg_str)


#--------------------------------------

'''
Monitor node - check if node is running
'''
class MonitoredNode(MonitoredItem):

    def __init__(self, name):
        self.init(name)
        self.category = "Node"
        self.counter = 0
        self.thresh = 3
        self.setState(WDNodeInfo.INIT, "system startup")

    def check(self, name_list):
        #rospy.logdebug("check node: %s", self.name)
        found = 0
        for name in name_list:
            if (name == self.name):
                found = 1

        if (found == 0):
            if (self.curr_state == WDNodeInfo.OK): 
                self.setState(WDNodeInfo.WARN, "not available")
                self.counter = 0;
                #rospy.logerr("WARNING")
            elif (self.curr_state == WDNodeInfo.WARN): 
                if (self.counter > self.thresh):
                    self.setState(WDNodeInfo.ERROR, "not running")
                    self.counter = 0
                    #rospy.logerr("ERROR")
                else:
                    self.counter = self.counter + 1
            elif (self.curr_state == WDNodeInfo.INIT):
                self.setState(WDNodeInfo.ERROR, "initial / not running")
            else:
                self.setState(WDNodeInfo.ERROR, "not running")
              
        else:
            if (self.curr_state == WDNodeInfo.ERROR):
                self.setState(WDNodeInfo.WARN, "available")
                self.counter = 0;
            elif (self.curr_state == WDNodeInfo.WARN):
                if (self.counter > self.thresh):
                    self.setState(WDNodeInfo.OK, "running")
                    self.counter = 0;
                    #rospy.logerr("OK")
                else:
                    self.counter = self.counter + 1
            elif (self.curr_state == WDNodeInfo.INIT):
                self.setState(WDNodeInfo.OK, "initial / running")

            elif (self.curr_state != WDNodeInfo.OK):
                self.setState(WDNodeInfo.ERROR, "not running")

        #self.print_state() 


    def get_status_json_format(self):
        auxstr = '{ "name" : "' + self.name + '", '
        auxstr = auxstr + '"state" : ' + self.get_state_str() + '", '
        auxstr = auxstr + '"description" : "' + self.state_change_description + '" }'
        return auxstr


    def get_msg_info(self):
        msginfo = WDNodeInfo()
        msginfo.name = self.name
        msginfo.status = self.curr_state
        msginfo.description = self.state_change_description
        return msginfo

#--------------------------------------

'''
Monitor topic - check if message reception rate is inside some valid range
'''
class MonitoredTopic(MonitoredItem):

    def __init__(self, name):
        self.init(name)
        self.category = "Topic"
        self.counter = 0
        self.thresh = 3
        self.setState(WDTopicInfo.UNDEF, "system startup")


    def set_topic_properties(self, minRate, maxRate, window_size=-1, ):
        """
        Periodically print the publishing rate of a topic to console until
        shutdown
        :param topic: topic name, ``str``
        :param window_size: number of messages to average over, -1 for infinite, ``int``
        """
        self.rate = -1.0;
        self.minRate = minRate;
        self.maxRate = maxRate;

        #@TODO check if topic is available - restart 
        self.rt = getROSTopicHz(self.name);

        #msg_class, real_topic, msg_eval = rostopic.get_topic_class(self.name, blocking=True)
        #self.rt = ROSTopicHz(real_topic, window_size)
        #self.sub = rospy.Subscriber(real_topic, msg_class, self.rt.callback_hz, self.name)        


    def check(self):
        #rospy.logdebug("check topic: %s", self.name)
        """
        check the average publishing rate
        if rate is -2 : no message received
        if rate is -1 : no new message received (current message is old)
        if rate is value >= 0 : check if it is inside valid range 
        """
        if (self.rt == None):
            self.rt = getROSTopicHz(self.name)
            if (self.rt == None):
                self.setState(MonitoredItem.stateErr, "topic not available")
                self.counter = 0
                return

        self.rate = self.rt.get_hz()

        if (self.rate == -2):
            self.setState(MonitoredItem.stateErr, "no message received")
            self.counter = 0

        elif (self.rate == -1):
            if (self.curr_state == MonitoredItem.stateOK):
                self.setState(MonitoredItem.stateWarn, "no new message")
                self.counter = 0
            elif (self.curr_state == MonitoredItem.stateWarn):
                self.counter = self.counter + 1
                if (self.counter > self.thresh):
                    self.setState(MonitoredItem.stateErr, "no new message")
                    self.counter = 0
            else:
                self.setState(MonitoredItem.stateErr, "no new message")
                self.counter = 0

        else:

            if ((self.rate >= self.minRate) and (self.rate <= self.maxRate)):
                rospy.logdebug("OK: INSIDE RANGE.");
                if (self.curr_state == MonitoredItem.stateErr):
                    self.setState(MonitoredItem.stateWarn, "inside rate / detected")
                    self.counter = 0
                elif (self.curr_state == MonitoredItem.stateWarn):
                    self.counter = self.counter + 1
                    if (self.counter > self.thresh):
                        self.setState(MonitoredItem.stateOK, "inside rate / stable")
                        self.counter = 0
                else:
                    self.setState(MonitoredItem.stateOK, "inside rate / stable")
                    self.counter = 0
            else:
                rospy.logdebug("ERROR: OUTSIDE RANGE.");
                if (self.curr_state == MonitoredItem.stateOK):
                    self.setState(MonitoredItem.stateWarn, "outside rate / detected")
                    self.counter = 0
                elif (self.curr_state == MonitoredItem.stateWarn):
                    self.counter = self.counter + 1
                    if (self.counter > self.thresh):
                        self.setState(MonitoredItem.stateErr, "outside rate / stable")
                        self.counter = 0
                else:
                    self.setState(MonitoredItem.stateErr, "outside rate / stable")
                    self.counter = 0

        #self.print_state()


    def get_status_json_format(self):
        auxstr = '{ ' 
        auxstr = auxstr + '"name" : "' + self.name + '", '
        auxstr = auxstr + '"state" : "' + self.get_state_str() + '", '
        auxstr = auxstr + '"description" : "' + self.state_change_description + '", '
        auxstr = auxstr + '"detail" : "Rate={0} (Min={1}/Max={2})" '.format(self.rate, self.minRate, self.maxRate)
        auxstr = auxstr + ' }'
        return auxstr


    def get_msg_info(self):
        msginfo = WDTopicInfo()
        msginfo.name = self.name
        msginfo.status = self.curr_state
        msginfo.description = self.state_change_description
        msginfo.detail = "Rate={0} (Min={1}/Max={2})".format(self.rate, self.minRate, self.maxRate)
        return msginfo


#--------------------------------------

'''
Monitor topic field - check if
- field value is equal to some expected value OR
- field value is inside some expected range
'''
class MonitoredTopicField(MonitoredItem):

    def __init__(self, name):
        self.init(name)
        self.category = "TopicField"
        self.fieldName = ""
        self.counter = 0
        self.thresh = 2
        self.setState(WDTopicFieldInfo.UNDEF, "system startup")
        self.useRange = False
        self.expectedValue = -1
        self.minValue = -1
        self.maxValue = -1
        self.value = None
        self.last_msg = None
        self.last_msg_time = None


    def set_field_properties(self, fieldName, expectedValue):
        self.fieldName = fieldName
        self.useRange = False
        self.expectedValue = expectedValue
        self.rt = getROSTopicHz(self.name);


    def set_field_properties_range(self, fieldName, minValue, maxValue):
        self.fieldName = fieldName
        self.useRange = True
        self.minValue = minValue
        self.maxValue = maxValue
        self.rt = getROSTopicHz(self.name);


    def _check_range_(self, value):
        #outside expected range : return -2
        #@TODO check types before comparing values with limits
        if ((value < self.minValue) or (value > self.maxValue)):
            return -2;
        return 0


    def _check_value_(self, value):
        #different than expected value : return -1
        #@TODO check types before comparing values with limits
        if (value != self.expectedValue):
            return -1;
        return 0


    def check(self):
        #rospy.logdebug("check topic field: %s %s", self.name, self.fieldName)

        if (self.rt == None):
            self.rt = getROSTopicHz(self.name)
            if (self.rt == None):
                self.setState(MonitoredItem.stateErr, "topic not available")
                self.counter = 0
                return

        msg, msg_time = self.rt.get_msg()
        #rospy.logerr(msg)

        #if no message received - result is -4
        #if message is old - result is 3
        #outside expected range - result is -2
        #different than expected value - result is -1
        #value is OK - result is 0
        result = -4
        if (msg != None):
            result = -3

            if (self.last_msg_time != msg_time):
                self.last_msg = msg
                self.last_msg_time = msg_time

                msg_eval = rostopic.msgevalgen("/"+self.fieldName)
                #rospy.logerr(msg_eval)
                self.value = msg_eval(msg)
                #rospy.logerr(self.value)
                if (self.useRange):
                    result = self._check_range_(self.value)
                else: 
                    result = self._check_value_(self.value)

        #rospy.logerr(result)
        if (result == -4):
            rospy.logdebug("NO VALUE - NO MESSAGE RECEIVED");
            self.setState(MonitoredItem.stateErr, "cannot check value / no message received")
            self.counter = 0
        elif (result == -3):
            rospy.logdebug("NO VALUE - OLD MESSAGE");
            self.setState(MonitoredItem.stateErr, "cannot check value / old message")
            self.counter = 0
        elif (result == -2):
            rospy.logdebug("VALUE OUTSIDE EXPECTED RANGE");
            if (self.curr_state == MonitoredItem.stateOK):
                self.setState(MonitoredItem.stateWarn, "value outside range / detected")
                self.counter = 0
            elif (self.curr_state == MonitoredItem.stateWarn):
                self.counter = self.counter + 1
                if (self.counter > self.thresh):
                    self.setState(MonitoredItem.stateErr, "value outside range / stable")
                    self.counter = 0
            else:
                self.setState(MonitoredItem.stateErr, "value outside range / stable")
                self.counter = 0
        elif (result == -1):
            rospy.logdebug("VALUE DIFFERENT THAN EXPECTED");
            if (self.curr_state == MonitoredItem.stateOK):
                self.setState(MonitoredItem.stateWarn, "value not valid / detected")
                self.counter = 0
            elif (self.curr_state == MonitoredItem.stateWarn):
                self.counter = self.counter + 1
                if (self.counter > self.thresh):
                    self.setState(MonitoredItem.stateErr, "value not valid / stable")
                    self.counter = 0
            else:
                self.setState(MonitoredItem.stateErr, "value not valid / stable")
                self.counter = 0
        else: 
            rospy.logdebug("VALUE IS OK");
            if (self.curr_state == MonitoredItem.stateErr):
                self.setState(MonitoredItem.stateWarn, "value valid / detected")
                self.counter = 0
            elif (self.curr_state == MonitoredItem.stateWarn):
                self.counter = self.counter + 1
                if (self.counter > self.thresh):
                    self.setState(MonitoredItem.stateOK, "value valid / stable")
                    self.counter = 0
            else:
                self.setState(MonitoredItem.stateOK, "value valid / stable")
                self.counter = 0

        #self.print_state()


    def print_state(self):
        msg_str = self.category + ":" + self.name + "/"  + self.fieldName 
        msg_str = msg_str +  " (state = " + self.get_state_str() + ")"
        msg_str = msg_str + " - " + self.state_change_description
        rospy.loginfo(msg_str)


    def get_status_json_format(self):
        auxstr = '{ ' 
        auxstr = auxstr + '"name" : "' + self.name + '", '
        auxstr = auxstr + '"state" : "' + self.get_state_str() + '", '
        auxstr = auxstr + '"description" : "' + self.state_change_description + '", '
        if (self.useRange):
            auxstr = auxstr + '"detail" : "Value={0} (Min={1}/Max={2})" '.format(self.value, self.minValue, self.maxValue)
        else:
            auxstr = auxstr + '"detail" : "Value={0} (Expected={1})" '.format(self.value, self.expectedValue)
        auxstr = auxstr + ' }'
        return auxstr

 
    def get_msg_info(self):
        msginfo = WDTopicFieldInfo()
        msginfo.topicName = self.name
        msginfo.fieldName = self.fieldName
        msginfo.status = self.curr_state
        msginfo.description = self.state_change_description
        if (self.useRange):
            msginfo.detail = "Value={0} (Min={1}/Max={2})".format(self.value, self.minValue, self.maxValue)
        else:
            msginfo.detail = "Value={0} (Expected={1})".format(self.value, self.expectedValue)
        return msginfo        


#==============================================================================

'''
Monitor node health state - listen to heart beat messsages (WDNodeInfo)
'''
class NodeHealthState:
    stateOK    = 0
    stateWarn  = 1
    stateErr   = 2
    stateNoMsg = 100
    stateUndef = 255

    def __init__(self, nodeName):
        self.name = nodeName
        self.status = WDNodeInfo.INIT
        self.description = 'System start up.'
        self.currHeartBeatMsg = None
        self.prevHeartBeatMsg = None
        self.isMsgAvailable = False
        self.timeoutWarn = HEART_BEAT_TIMEOUT_WARN
        self.timeoutError = HEART_BEAT_TIMEOUT_ERROR
        self.lastHeartBeatTime = rospy.get_time()
        self.sub = rospy.Subscriber(nodeName + '/wdHeartBeat', WDNodeInfo, self.heartBeatMsgCallback )

    def heartBeatMsgCallback(self, msg):
        #rospy.logerr('HEAT BEAT FROM ' + self.name + ' - ' + msg.description);
        self.setMsg(msg)

    def setMsg(self, heartBeatMsg):
        self.prevHeartBeatMsg = self.currHeartBeatMsg
        self.currHeartBeatMsg = heartBeatMsg
        self.isMsgAvailable = True
        self.lastHeartBeatTime = rospy.get_time()

    def check(self):
        currTime = rospy.get_time()

        #Check for timeouts
        if ((currTime - self.lastHeartBeatTime) > self.timeoutError):
            self.status = WDNodeInfo.HUNG
            self.description = 'ERROR: No heart beat messages received.'
            return self.status

        if ((currTime - self.lastHeartBeatTime) > self.timeoutWarn):
            self.status = WDNodeInfo.WARN
            self.description = 'WARNING: No heart beat messages received.'
            return self.status

        #No new message available - just use last result
        if (not self.isMsgAvailable):
            return self.status

        #Message is available process content
        self.status = self.currHeartBeatMsg.status
        self.description = self.currHeartBeatMsg.description
        self.isMsgAvailable = False
        #self.print_state()
        return self.status


    def print_state(self):
        if (self.status == WDNodeInfo.OK):
            rospy.logerr('HEART BEAT [{0}] : OK'.format(self.name))

        elif (self.status == WDNodeInfo.INIT):
            rospy.logerr('HEART BEAT [{0}] : INIITAL'.format(self.name))

        elif (self.status == WDNodeInfo.WARN):
            rospy.logerr('HEART BEAT [{0}] : WARNING'.format(self.name))
            
        elif (self.status == WDNodeInfo.ERROR):
            rospy.logerr('HEART BEAT [{0}] : ERROR'.format(self.name))

        elif (self.status == WDNodeInfo.HUNG):
            rospy.logerr('HEART BEAT [{0}] : HUNG / NO MESSAGES'.format(self.name))

        elif (self.status == WDNodeInfo.KILLED):
            rospy.logerr('HEART BEAT [{0}] : KILLED (by Process Manager)'.format(self.name))

        elif (self.status == WDNodeInfo.DEAD):
            rospy.logerr('HEART BEAT [{0}] : DEAD'.format(self.name))

        elif (self.status == WDNodeInfo.UNDEF):
            rospy.logerr('HEART BEAT [{0}] : UNDEF'.format(self.name))

        else:
            rospy.logerr('HEART BEAT [{0}] : UNKNOWN STATE'.format(self.name))


    def get_msg_info(self):
        msginfo = WDNodeInfo()
        msginfo.name = self.name
        msginfo.status = self.status
        msginfo.description = self.description
        return msginfo


    def update_msg_info(self, msginfo):
        #This routine changes the content of the message
        #But the message does not change the state of the internal health status 
        #Message content update only happens when the internal status is "worse"
        #than the message status
        if (self.status == WDNodeInfo.OK):
            #Do nothing - just return
            return msginfo
            
        elif (self.status == WDNodeInfo.INIT):
            #Do nothing - just return
            return msginfo
            
        elif (self.status == WDNodeInfo.WARN):
            if ((msginfo.status == WDNodeInfo.OK) or
                (msginfo.status == WDNodeInfo.INIT)):
                #update status and description
                msginfo.status      = self.status
                msginfo.description = self.description            
            elif (msginfo.status == WDNodeInfo.WARN):
                #same level - append information
                msginfo.description = msginfo.description + ' / '+ self.description
            return msginfo

        elif (self.status == WDNodeInfo.ERROR):
            if ((msginfo.status == WDNodeInfo.OK) or
                (msginfo.status == WDNodeInfo.INIT) or
                (msginfo.status == WDNodeInfo.WARN)):
                #update status and description
                msginfo.status      = self.status
                msginfo.description = self.description            
            elif (msginfo.status == WDNodeInfo.ERROR):
                #same level - append information
                msginfo.description = msginfo.description + ' / '+ self.description
            return msginfo
        
        elif ((self.status == WDNodeInfo.HUNG) or
              (self.status == WDNodeInfo.KILLED) or
              (self.status == WDNodeInfo.DEAD)):
            #update status and description
            msginfo.status      = self.status
            msginfo.description = self.description            
            return msginfo

        elif (self.status == WDNodeInfo.UNDEF):
            #@TODO need to evaluate if this is the best approach for undefined state
            #Do nothing - just return
            return msginfo
        
        else:
            #Something not consistent in 
            msginfo.status      = WDNodeInfo.ERROR
            msginfo.description = 'ERROR: undefined internal state in health monitoring'
            return msginfo




#==============================================================================
'''
Auxiliar things created to publish watchdog information in JSON format through
a simple HTTP Server - @TODO: check if it is necessary
'''

PORT_NUMBER = 8080

#This class will handles any incoming request from
#the browser 
class WatchdogStatusHTTPHandler(BaseHTTPRequestHandler):
	
    #Handler for the GET requests
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        # Send the html message
        auxstr = get_monitored_status_json_str()
        rospy.loginfo(auxstr)
        self.wfile.write(auxstr)
        return

#This class is a thread that will serve http requests
class WatchdogStatusHTTPThreadClass(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.keepRunning = True
        self.web_server = HTTPServer(('', PORT_NUMBER), WatchdogStatusHTTPHandler)

    def run(self):
        while (self.keepRunning):
            if (self.web_server != None):
                self.web_server.handle_request()
                now = datetime.datetime.now()
                print "%s says Hello World at time: %s" % (self.getName(), now)

    def stop(self):
        if (self.web_server != None):
            self.web_server.socket.close()
        self.keepRunning = False

#===============================================================================

def get_items_status_json_str(list_name, items_list):
    auxstr = '{ "' + list_name + '" : [ '
    size = len(items_list)
    counter = 0
    for item in items_list:
        auxstr = auxstr + item.get_status_json_format();
        counter = counter + 1
        if (counter < size):
            auxstr = auxstr + ", "
    auxstr = auxstr + '] }'
    return auxstr


def get_monitored_status_json_str():
    global g_objlist_nodes
    global g_objlist_topics
    global g_objlist_fields    
    auxstr = '{'
    auxstr = auxstr + get_items_status_json_str('nodesStatus', g_objlist_nodes) + ',';
    auxstr = auxstr + get_items_status_json_str('topicsStatus', g_objlist_topics) + ',';
    auxstr = auxstr + get_items_status_json_str('topicFieldsStatus', g_objlist_fields) + ',';
    auxstr = auxstr + '}'
    return auxstr




#===============================================================================

'''
Load parameters from launch file:
1) List of nodes (name)
2) List of topics (name and rate)
3) List of topic field (topic name, field name, expected value or expected value range)
'''
def load_params():
    global g_objlist_nodes
    global g_objlist_topics
    global g_objlist_fields
    global g_objlist_health  

    rospy.logdebug("load_params()")

    #load nodes that will be monitored
    for i in range(MAX_NUM_MONITORED_NODES):
        param_name = "/watchdog/nodeList/{0}".format(i+1)
        #rospy.logerr(param_name)
        if (rospy.has_param(param_name)):
            node_name = rospy.get_param(param_name)
            if (node_name):
                #rospy.logerr("NODE[{0}]:".format(i+1) + node_name)
                aux = MonitoredNode(node_name)
                g_objlist_nodes.append(aux)

    #load messages that will be monitored and frequency range
    for i in range(MAX_NUM_MONITORED_TOPICS):
        param_name = "/watchdog/topicList/{0}/name".format(i+1)
        #rospy.logerr(param_name)
        if (rospy.has_param(param_name)):
            topic_name = rospy.get_param(param_name)
            if (topic_name):
                minRate = -1;
                param_name = "/watchdog/topicList/{0}/minRate".format(i+1)
                if (rospy.has_param(param_name)):
                    minRate = rospy.get_param(param_name)

                maxRate = -1;
                param_name = "/watchdog/topicList/{0}/maxRate".format(i+1)
                if (rospy.has_param(param_name)):
                    maxRate = rospy.get_param(param_name)

                #rospy.logerr("TOPIC[{0}]:name=".format(i+1) + topic_name)
                #rospy.logerr("TOPIC[{0}]:minRate={1}".format(i+1, minRate))
                #rospy.logerr("TOPIC[{0}]:maxRate={1}".format(i+1, maxRate))

                aux = MonitoredTopic(topic_name)
                aux.set_topic_properties(minRate, maxRate, 200)
                g_objlist_topics.append(aux)

    #load message fields that will be monitored and valid value or values range
    for i in range(MAX_NUM_MONITORED_FIELDS):
        topic_name = ''
        field_name = ''
        param_name = "/watchdog/fieldList/{0}/topicName".format(i+1)
        if (rospy.has_param(param_name)):
            topic_name = rospy.get_param(param_name)
        param_name = "/watchdog/fieldList/{0}/fieldName".format(i+1)
        if (rospy.has_param(param_name)):
            field_name = rospy.get_param(param_name)

        if (topic_name != '') and (field_name != ''):
            useRange = False
            expectedValue = -1
            minValue = -1
            maxValue = -1
            param_name = "/watchdog/fieldList/{0}/expectedValue".format(i+1)
            if (rospy.has_param(param_name)):
                expectedValue = rospy.get_param(param_name)
                useRange = False
            else:
                param_name = "/watchdog/topicList/{0}/minValue".format(i+1)
                if (rospy.has_param(param_name)):
                    minValue = rospy.get_param(param_name)
                param_name = "/watchdog/topicList/{0}/maxValue".format(i+1)
                if (rospy.has_param(param_name)):
                    maxValue = rospy.get_param(param_name)
                useRange = True
                
            #rospy.logerr("FIELD[{0}]:topic=".format(i+1) + topic_name + " | field=" + field_name)
            aux = MonitoredTopicField(topic_name)
            if (useRange):
                #rospy.logerr("FIELD[{0}]:minValue={1}".format(i+1, minValue))
                #rospy.logerr("FIELD[{0}]:maxValue={1}".format(i+1, maxValue))
                aux.set_field_properties_range(field_name, minValue, maxValue)
            else:
                #rospy.logerr("FIELD[{0}]:expectedValue={1}".format(i+1, expectedValue))
                aux.set_field_properties(field_name, expectedValue)

            g_objlist_fields.append(aux)

    #load nodes that will have heart beat monitored
    for i in range(MAX_NUM_MONITORED_NODES):
        param_name = "/watchdog/nodeHealthList/{0}".format(i+1)
        #rospy.logerr(param_name)
        if (rospy.has_param(param_name)):
            node_name = rospy.get_param(param_name)
            if (node_name):
                #rospy.logerr("NODE[{0}]:".format(i+1) + node_name)
                aux = NodeHealthState(node_name)
                g_objlist_health.append(aux)


#===============================================================================

# Check nodes - list nodes and check if they are up and running
# It does not check if node is functioning properly

def check_nodes():
    global g_objlist_nodes

    rospy.logdebug("check_nodes()")
    num_ok = 0
    num_warn = 0
    num_err = 0
    num_undef = 0   

    node_names = rosnode.get_node_names(None)
    node_names.sort()
    for node in g_objlist_nodes:
        node.check(node_names);

        if ((node.curr_state == WDNodeInfo.OK) or
            (node.curr_state == WDNodeInfo.INIT)):
            num_ok = num_ok + 1
        elif (node.curr_state == WDNodeInfo.WARN):
            num_warn = num_warn + 1
        elif (node.curr_state == WDNodeInfo.ERROR):
            num_err = num_err + 1
        elif ((node.curr_state == WDNodeInfo.HUNG) or
              (node.curr_state == WDNodeInfo.KILLED) or
              (node.curr_state == WDNodeInfo.DEAD)):
            num_err = num_err + 1
        else:
            num_undef = num_undef + 1

    return [num_ok, num_warn, num_err, num_undef]


# Check topic - subscribe to a topic and measure reception frequency

def check_messages_frequency():
    global g_objlist_topics

    rospy.logdebug("check_messages_frequency()")
    num_ok = 0
    num_warn = 0
    num_err = 0
    num_undef = 0   

    for topic in g_objlist_topics:
        topic.check();

        if (topic.curr_state == WDTopicInfo.OK):
            num_ok = num_ok + 1
        elif (topic.curr_state == WDTopicInfo.WARN):
            num_warn = num_warn + 1
        elif (topic.curr_state == WDTopicInfo.ERROR):
            num_err = num_err + 1
        else:
            num_undef = num_undef + 1

    return [num_ok, num_warn, num_err, num_undef]


# Receive message and check one message field
# It checks if content is what is expected for normal operation

def check_messages_content():
    global g_objlist_fields    

    rospy.logdebug("check_messages_content()")
    num_ok = 0
    num_warn = 0
    num_err = 0
    num_undef = 0   

    for field in g_objlist_fields:
        field.check();

        if (field.curr_state == WDTopicFieldInfo.OK):
            num_ok = num_ok + 1
        elif (field.curr_state == WDTopicFieldInfo.WARN):
            num_warn = num_warn + 1
        elif (field.curr_state == WDTopicFieldInfo.ERROR):
            num_err = num_err + 1
        else:
            num_undef = num_undef + 1

    return [num_ok, num_warn, num_err, num_undef]


# Check node health state through heart beat message

def check_nodes_health():
    global g_objlist_health

    rospy.logdebug("check_nodes_health()")
    num_ok = 0
    num_warn = 0
    num_err = 0
    num_undef = 0   

    for node in g_objlist_health:
        node.check();
        if ((node.status == WDNodeInfo.OK) or
            (node.status == WDNodeInfo.INIT)):
            num_ok = num_ok + 1
        elif (node.status == WDNodeInfo.WARN):
            num_warn = num_warn + 1
        elif (node.status == WDNodeInfo.ERROR):
            num_err = num_err + 1
        elif ((node.status == WDNodeInfo.HUNG) or
              (node.status == WDNodeInfo.KILLED) or
              (node.status == WDNodeInfo.DEAD)):
            num_err = num_err + 1
        else:
            num_undef = num_undef + 1

    return [num_ok, num_warn, num_err, num_undef]

'''
        if (node.curr_state == WDNodeInfo.OK):
            num_ok = num_ok + 1
        elif (node.curr_state == WDNodeInfo.WARN):
            num_warn = num_warn + 1
        elif (node.curr_state == WDNodeInfo.ERROR):
            num_err = num_err + 1
        else:
            num_undef = num_undef + 1
'''



#===============================================================================

# publish overall status information - can be used by process manager / system manager 

def publish_watchdog_info(overall_status):
    global g_objlist_nodes
    global g_objlist_topics
    global g_objlist_fields
    global g_objlist_health
    global g_pub_watchdog_info

    rospy.logdebug("publish_watchdog_info()")
    if (g_pub_watchdog_info == None):
        rospy.logerr("Watchdog publisher not available.")
        return

    #Fill watchdog message
    auxinfo = WatchdogInfo()
    auxinfo.header.stamp = rospy.get_rostime()
    auxinfo.status = overall_status

    #include all nodes names that are running
    node_names = rosnode.get_node_names(None)
    node_names.sort()
    for name in node_names:
        auxinfo.nodeNames.append(name)

    #include all topics that are being published
    master = rosgraph.Master('/rostopic')
    state = master.getSystemState()
    pubs, subs, aux = state
    for pub in pubs:
        auxinfo.topicNames.append(pub[0])
    
    #include info about each monitored node
    for node in g_objlist_nodes:
        msg = node.get_msg_info()
        auxinfo.nodeList.append(msg)

    #include info about each monitored topic
    for topic in g_objlist_topics:
        msg = topic.get_msg_info()
        auxinfo.topicList.append(msg)

    #include info about each monitored topic field
    for field in g_objlist_fields:
        msg = field.get_msg_info()
        auxinfo.topicFieldList.append(msg)

    #include info about each monitored node health state
    #this will update previous information about the node
    for nodeHealth in g_objlist_health:
        found = False
        for nodeInfo in auxinfo.nodeList:
            #rospy.logerr(nodeHealth.name + ' - ' + nodeInfo.name)
            if (nodeHealth.name == nodeInfo.name):
               #remove from list, update info and then add to the list again 
               auxinfo.nodeList.remove(nodeInfo)
               nodeInfo = nodeHealth.update_msg_info(nodeInfo)
               auxinfo.nodeList.append(nodeInfo)
               found = True

        if (found == False):
            nodeInfo = nodeHealth.get_msg_info()
            auxinfo.nodeList.append(nodeInfo)


    #finally publish content
    g_pub_watchdog_info.publish(auxinfo)


#===============================================================================


def callback_shutdown():
    rospy.loginfo("Shutdown requested!")



#===============================================================================
# MAIN - execution entry point

def main_watchdog():
    global g_pub_watchdog_info
    global g_objlist_nodes
    global g_objlist_topics    
    global g_objlist_fields    
    global g_objlist_health
    global g_objlist_rostopichz

    g_pub_watchdog_info = None
    g_objlist_nodes = []
    g_objlist_topics = []
    g_objlist_fields = []
    g_objlist_health = []
    g_objlist_rostopichz = []
    
    rospy.loginfo("Starting WatchDog...");

    rospy.init_node('watchdog', anonymous=False, log_level=rospy.DEBUG, disable_signals=False)

    rospy.on_shutdown(callback_shutdown)

    #create publisher for watchdog monitor information
    g_pub_watchdog_info = rospy.Publisher("watchdogInfo", WatchdogInfo)

    #load parameters - configure what will be monitored
    load_params()


    initInterval = 0
    if (rospy.has_param("/watchdog/initInterval")):
        initInterval = rospy.get_param("/watchdog/initInterval")

    startTime = rospy.get_rostime()


    scanRate = WATCHDOG_SCAN_RATE_HZ
    if (rospy.has_param("/watchdog/scanRateHz")):
        scanRate = rospy.get_param("/watchdog/scanRateHz")

    wdrate = rospy.Rate(scanRate)
    while not rospy.is_shutdown():
        #perform checking procedure
        #check nodes
        result_nodes = check_nodes()
        #check messages frequency - within a range
        result_topics = check_messages_frequency()
        #check messages content - values inside a range
        result_fields = check_messages_content()
        #check nodes health - based on heart beat messages
        result_health = check_nodes_health()
    
        overall_ok = result_nodes[0] + result_topics[0] + result_fields[0] + result_health[0]
        overall_warn = result_nodes[1] + result_topics[1] + result_fields[1] + result_health[1]
        overall_error = result_nodes[2] + result_topics[2] + result_fields[2] + result_health[2]
        overall_undef = result_nodes[3] + result_topics[3] + result_fields[3] + result_health[3]
    
        overall_status = WatchdogInfo.INIT
        if ((rospy.get_rostime() - startTime).secs > initInterval):
            if (overall_error > 0):
                overall_status = WatchdogInfo.FAULT
                rospy.logerr("*** OVERALL STATUS : FAULT ***");
            elif (overall_warn > 0):
                overall_status = WatchdogInfo.WARN
                rospy.logerr("*** OVERALL STATUS : WARNING ***");
            else:
                overall_status = WatchdogInfo.ALIVE

        if (overall_status == WatchdogInfo.INIT):
            rospy.loginfo("*** OVERALL STATUS : INIT ***");

        #publish watchdog information   
        publish_watchdog_info(overall_status)

        wdrate.sleep()


if __name__ == '__main__':
    try:
        main_watchdog()
    except rospy.ROSInterruptException:
        pass

