/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include <ros/ros.h>
#include <riverine_watchdog/sensors.h>
using namespace CA;


int main(int argc, char **argv)
{
  ros::init(argc, argv, "watchdogProcess");
  ros::NodeHandle n("~");
  ros::Rate loop(1.0);
  WatchdogProcesser wp(n);
  if(!wp.setup())
  { 
      ROS_ERROR_STREAM("Was not able to setup watchdog process");
      return -1;	      
  } 
  ros::spin();
  return 0;
}


