/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include <riverine_watchdog/sensors.h>

#include <riverine_watchdog/HealthStatus.h>
#include <boost/foreach.hpp>
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <riverine_watchdog/prcInformation.h>


using namespace CA;

WatchdogProcesser::WatchdogProcesser(ros::NodeHandle &nh):
initialDelay(0),
setup_(false),
armed_(true),
level(-1),
n(nh),
startTime(ros::Time::now())
{
  sub        = n.subscribe("/watchdog", 10, &CA::WatchdogProcesser::processWatchdog,this);
  subCommand = n.subscribe("/watchdogCommand", 10, &CA::WatchdogProcesser::processCommand,this);
  timer = n.createTimer(ros::Duration(1.0),&CA::WatchdogProcesser::processTimer,this);
  statusPub = n.advertise<riverine_watchdog::HealthStatus>("/watchdogStatus",2);
  healthStatus.header.frame_id = "";
  healthStatus.header.seq   = 0;
  healthStatus.overallStatus   = riverine_watchdog::HealthStatus::ALIVE;
  healthStatus.msg = "Init.";

}

void WatchdogProcesser::processTimer(const ros::TimerEvent &ev)
{
  
  //Check the list of processes and frequencies to see if they are still alive
  ros::Time nowTime = ros::Time::now();
  int overallStatus = riverine_watchdog::HealthStatus::ALIVE;
  
  typedef std::map < std::string, riverine_watchdog::WatchedElement> wle;
  healthStatus.processes.clear();      
  healthStatus.processes.reserve(elements.size());

  BOOST_FOREACH(wle::value_type &elem, elements)
    {
      std::ostringstream strstrm;
    
      if(!PRCInformation::getStatistics(elem.second.processStatistics,elem.second.element.pid))
	{
	  strstrm<<"Was not able to get process statistics. Not on local machine?, ";
	}
      
      //Calculate and update timers and frequencies:
      double duration = fabs((nowTime - elem.second.element.eventTime).toSec());
      double durationMsg = fabs((elem.second.element.eventTime - elem.second.lastResponse).toSec()) ;
      if(durationMsg >0)
	{
	  elem.second.updateFrequency = elem.second.updateFrequency * 0.7 +  0.3/durationMsg;
	}
      //Update decision logic

      if(duration > elem.second.element.maxInitDelay_s)
	{
	  elem.second.element.status = riverine_watchdog::WatchdogElement::FAULT;
	  overallStatus = riverine_watchdog::HealthStatus::FAULT;
	  strstrm<<"FAULT: Max init timer expired, "; 
	}
      if(elem.second.element.status != riverine_watchdog::WatchdogElement::INIT)
	{
	  if(elem.second.updateFrequency <= elem.second.element.updateFrequency_hz * 0.8)
	    {
	      elem.second.element.status = riverine_watchdog::WatchdogElement::WARN;
	      if(overallStatus != riverine_watchdog::HealthStatus::FAULT)
		overallStatus = riverine_watchdog::HealthStatus::WARN;
	      strstrm<<"WARN: Update frequency does not match, "; 

	    }
	  if(elem.second.updateFrequency <= elem.second.element.updateFrequency_hz*0.5)
	    {
	      elem.second.element.status = riverine_watchdog::WatchdogElement::FAULT;
	      overallStatus = riverine_watchdog::HealthStatus::FAULT;
	      strstrm<<"FAULT: Too slow frequency, "; 
	 
	    }
	  if(elem.second.element.status == riverine_watchdog::WatchdogElement::FAULT)
	    {
	      elem.second.element.status = riverine_watchdog::WatchdogElement::FAULT;
	      overallStatus = riverine_watchdog::HealthStatus::FAULT;
	      strstrm<<"FAULT: Self reported fault. Too slow? "; 	  
	    }
	}
      elem.second.msg = strstrm.str();
      healthStatus.processes.push_back(elem.second);
    }
  std::ostringstream strstrm;
  if(armed_)
    {
      if((nowTime - startTime).toSec() < initialDelay)
	{
	  healthStatus.overallStatus = riverine_watchdog::HealthStatus::INIT;
	  strstrm<<"Still waiting for system to come up. Keeping state at INIT.\n";
	}
      else
	{
	  if(healthStatus.overallStatus != riverine_watchdog::HealthStatus::FAULT)
	    {
	      healthStatus.overallStatus = overallStatus;
	    }
	  
	  if(healthStatus.overallStatus == riverine_watchdog::HealthStatus::FAULT)
	    {
	      strstrm<<"Watchdog was TRIGGERED.\n";
	    }else
	    {
	      strstrm<<"Watchdog is healthy.\n";
	    }
	}
      strstrm<<"Watchdog ARMED.\n";
    }else
    {
      healthStatus.overallStatus = riverine_watchdog::HealthStatus::INIT;
      strstrm<<"Watchdog DISARMED.\n";
    }
  healthStatus.header.stamp = ros::Time::now();
  healthStatus.msg = strstrm.str();
  statusPub.publish(healthStatus);
}

void WatchdogProcesser::processWatchdog(const riverine_watchdog::WatchdogStatus::ConstPtr &msg)
{
  std::string index = msg->element.name;
  bool found = (elements.find(index) != elements.end());
  bool levelval =msg->element.level == level ;


  if( found && levelval)
    {
      elements[index].lastResponse = elements[index].element.eventTime;
      elements[index].element = msg->element;    
    }
}

void WatchdogProcesser::processCommand(const riverine_watchdog::WatchdogCommand::ConstPtr &msg)
{
  if(level == msg->level)
    {
      switch(msg->command)
	{
	case riverine_watchdog::WatchdogCommand::ARM:
	  if(!armed_)
	    {
	      armed_ = true;
	      reset();
	      
	    }
	  break;
	case riverine_watchdog::WatchdogCommand::DISARM:
	  armed_ = false;
	  break;
	default:
	  break;
	};
    }
}

bool WatchdogProcesser::setup()
{
  std::string nodes;
  //Read the list of processes to supervise and the level.
  setup_ = true;
  setup_ = setup_ && n.getParam("watchdog/level",level);
  healthStatus.level = level;
  setup_ = setup_ && n.getParam("watchdog/initialDelay_s",initialDelay);
  setup_ = setup_ && n.getParam("watchdog/nodes",nodes);
  std::vector<std::string> strs;
  boost::split(strs,nodes,boost::is_any_of(","));
  riverine_watchdog::WatchedElement tempElem;
  tempElem.id = 0;
  ros::Time nt = ros::Time::now();
  startTime = nt;
  BOOST_FOREACH(std::string &nodeName, strs)
    {
      boost::trim(nodeName);
      if(nodeName.length()>0)
	{
	  tempElem.id++;
	  tempElem.element.name = nodeName;
	  tempElem.lastResponse = nt;
	  tempElem.element.level = level;
	  tempElem.element.eventTime = nt;
	  tempElem.element.expectedFrequency_hz = 0.05;
	  tempElem.element.updateFrequency_hz = 0.05;
	  tempElem.updateFrequency = 0.1;
	  tempElem.element.maxMemory_Mb = 1000;
	  tempElem.element.status = riverine_watchdog::WatchdogElement::WARN;
	  elements[nodeName] = tempElem;
	}
    }
  return setup_;
 }

void WatchdogProcesser::reset()
{
    ros::Time nt = ros::Time::now();
    startTime = nt;
  typedef std::map < std::string, riverine_watchdog::WatchedElement> wle;
    BOOST_FOREACH(wle::value_type &elem, elements)
    {
      elem.second.element.eventTime = nt;
      elem.second.lastResponse = nt;
      elem.second.element.status = riverine_watchdog::WatchdogElement::WARN;
      elem.second.msg = "reset";
    }
    healthStatus.overallStatus = riverine_watchdog::HealthStatus::INIT;
    healthStatus.msg = "reset";
}

