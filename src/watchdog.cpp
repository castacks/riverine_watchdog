/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include <riverine_watchdog/watchdog.h>
#include <unistd.h>

using namespace CA;
PetWatchdog::PetWatchdog():
  setup_(false)
{}
bool PetWatchdog::setup(ros::NodeHandle &n)
{
  setup_ = true;
  petPub = n.advertise<riverine_watchdog::WatchdogStatus>("/watchdog",2);
  double temp;
  setup_ = setup_ && n.getParam("watchdog/updateFrequency_hz",temp);
  status.element.updateFrequency_hz = temp;
  if(temp == 0.0)
    setup_ = false;
  else
    updateDelay_ = ros::Duration(1.0/temp);
  setup_ = setup_ && n.getParam("watchdog/expectedFrequency_hz",temp);
  status.element.expectedFrequency_hz = temp;
  setup_ = setup_ && n.getParam("watchdog/maxInitDelay_s",temp);
  status.element.maxInitDelay_s = temp;
  int level = 0;
  setup_ = setup_ && n.getParam("watchdog/level",level);
  status.element.level = level;
  setup_ = setup_ && n.getParam("watchdog/maxMemory_mb",level);
  status.element.maxMemory_Mb = level;
  status.element.name = ros::this_node::getName();
  status.header.seq = 0;
  status.header.frame_id = "";
  status.element.pid = getpid();
  last_ = ros::Time::now();
  lastActual_ = last_;
  status.element.actualFrequency_hz = 1.0;
  if(setup_)
    init();
  return setup_;
}




void PetWatchdog::sendMessage(int stat)
{
  if(setup_)
    {
      status.element.status = stat;
      status.header.stamp = ros::Time::now();
      status.element.eventTime = status.header.stamp;
      status.header.seq++;
      petPub.publish(status);
    }
  else
    {
      ROS_ERROR_STREAM("WATCHDOG NOT SETUP");
    }
}

void PetWatchdog::alive()
{
  ros::Time curr = ros::Time::now();
  ros::Duration d = curr - last_;
  double callFreq = callFrequency((curr - lastActual_).toSec());
  lastActual_ = curr;
  if(callFreq < status.element.expectedFrequency_hz)
    {
      sendMessage(riverine_watchdog::WatchdogElement::WARN);
      last_ = curr;
    }
  else if(callFreq < 0.5 * status.element.expectedFrequency_hz)
    {
      sendMessage(riverine_watchdog::WatchdogElement::FAULT);
      last_ = curr;
    }
  else
    {
      if( d >= updateDelay_)
	{
	  sendMessage(riverine_watchdog::WatchdogElement::ALIVE);
	  last_ = curr;
	}
    }

}

void PetWatchdog::warn()
{
  ros::Time curr = ros::Time::now();
  ros::Duration d = curr - last_;
  double callFreq = callFrequency((curr - lastActual_).toSec());
  lastActual_ = curr;
  if(callFreq < 0.5 * status.element.expectedFrequency_hz)
    {
      sendMessage(riverine_watchdog::WatchdogElement::FAULT);
      last_ = curr;
    }
  else
    {
      if( d >= updateDelay_)
	{
	  sendMessage(riverine_watchdog::WatchdogElement::WARN);
	  last_ = curr;
	}
    }
}

void PetWatchdog::fault()
{
  ros::Time curr = ros::Time::now();
  ros::Duration d = curr - last_;
  sendMessage(riverine_watchdog::WatchdogElement::FAULT);
  last_ = curr;
}

void PetWatchdog::init()
{
  ros::Time curr = ros::Time::now();
  ros::Duration d = curr - last_;
  sendMessage(riverine_watchdog::WatchdogElement::INIT);
  last_ = curr;
}

double PetWatchdog::callFrequency(double duration)
{
  if(duration >0.0)
    {
      status.element.actualFrequency_hz = status.element.actualFrequency_hz * 0.7 + 0.3/duration;
    }
  return status.element.actualFrequency_hz;
}
