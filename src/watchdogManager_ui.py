# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'watchdogManager.ui'
#
# Created: Wed Aug 15 13:49:35 2012
#      by: PyQt4 UI code generator 4.9.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(634, 499)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_3 = QtGui.QLabel(self.centralwidget)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_2.addWidget(self.label_3)
        self.levelCombo = QtGui.QComboBox(self.centralwidget)
        self.levelCombo.setObjectName(_fromUtf8("levelCombo"))
        self.levelCombo.addItem(_fromUtf8(""))
        self.levelCombo.addItem(_fromUtf8(""))
        self.levelCombo.addItem(_fromUtf8(""))
        self.levelCombo.addItem(_fromUtf8(""))
        self.horizontalLayout_2.addWidget(self.levelCombo)
        self.horizontalLayout_3.addLayout(self.horizontalLayout_2)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label = QtGui.QLabel(self.centralwidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout.addWidget(self.label)
        self.overallStatus = QtGui.QLabel(self.centralwidget)
        self.overallStatus.setObjectName(_fromUtf8("overallStatus"))
        self.horizontalLayout.addWidget(self.overallStatus)
        self.horizontalLayout_3.addLayout(self.horizontalLayout)
        self.verticalLayout_3.addLayout(self.horizontalLayout_3)
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.verticalLayout = QtGui.QVBoxLayout(self.groupBox)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.elementTable = QtGui.QTableWidget(self.groupBox)
        self.elementTable.setObjectName(_fromUtf8("elementTable"))
        self.elementTable.setColumnCount(0)
        self.elementTable.setRowCount(0)
        self.horizontalLayout_4.addWidget(self.elementTable)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        self.verticalLayout_3.addWidget(self.groupBox)
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.killNodeButton = QtGui.QPushButton(self.centralwidget)
        self.killNodeButton.setObjectName(_fromUtf8("killNodeButton"))
        self.horizontalLayout_5.addWidget(self.killNodeButton)
        self.enableWatchdogButton = QtGui.QPushButton(self.centralwidget)
        self.enableWatchdogButton.setObjectName(_fromUtf8("enableWatchdogButton"))
        self.horizontalLayout_5.addWidget(self.enableWatchdogButton)
        self.disableWatchdogButton = QtGui.QPushButton(self.centralwidget)
        self.disableWatchdogButton.setObjectName(_fromUtf8("disableWatchdogButton"))
        self.horizontalLayout_5.addWidget(self.disableWatchdogButton)
        self.verticalLayout_3.addLayout(self.horizontalLayout_5)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 634, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "Watchdog Manager", None, QtGui.QApplication.UnicodeUTF8))
        self.label_3.setText(QtGui.QApplication.translate("MainWindow", "Level to Display:", None, QtGui.QApplication.UnicodeUTF8))
        self.levelCombo.setItemText(0, QtGui.QApplication.translate("MainWindow", "All Levels", None, QtGui.QApplication.UnicodeUTF8))
        self.levelCombo.setItemText(1, QtGui.QApplication.translate("MainWindow", "Level 1", None, QtGui.QApplication.UnicodeUTF8))
        self.levelCombo.setItemText(2, QtGui.QApplication.translate("MainWindow", "Level 2", None, QtGui.QApplication.UnicodeUTF8))
        self.levelCombo.setItemText(3, QtGui.QApplication.translate("MainWindow", "Level 3", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("MainWindow", "Current Overall Status:", None, QtGui.QApplication.UnicodeUTF8))
        self.overallStatus.setText(QtGui.QApplication.translate("MainWindow", "INIT", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("MainWindow", "Nodes", None, QtGui.QApplication.UnicodeUTF8))
        self.killNodeButton.setText(QtGui.QApplication.translate("MainWindow", "Kill Node", None, QtGui.QApplication.UnicodeUTF8))
        self.enableWatchdogButton.setText(QtGui.QApplication.translate("MainWindow", "Enable Watchdog", None, QtGui.QApplication.UnicodeUTF8))
        self.disableWatchdogButton.setText(QtGui.QApplication.translate("MainWindow", "Disable Watchdog", None, QtGui.QApplication.UnicodeUTF8))

