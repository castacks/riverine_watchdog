#!/usr/bin/env python
import roslib; roslib.load_manifest('watchdog')
import rospy
import sys
from subprocess import call

from watchdog.msg import HealthStatus, WatchedElement, WatchdogElement,WatchdogCommand
from std_msgs.msg import String
from PyQt4 import QtGui,QtCore
from watchdogManager_ui import *
import signal

class rosConnector(object):
    def __init__(self,ui):
	self.ui_local = ui
        self.ui_local.elementTable.setColumnCount(8);
        self.ui_local.elementTable.setHorizontalHeaderItem(0, QtGui.QTableWidgetItem("Level"))
        self.ui_local.elementTable.setColumnWidth(0, 40)
        self.ui_local.elementTable.setHorizontalHeaderItem(1, QtGui.QTableWidgetItem("Status"))
        self.ui_local.elementTable.setColumnWidth(1, 40)
        self.ui_local.elementTable.setHorizontalHeaderItem(2, QtGui.QTableWidgetItem("Name"))
        self.ui_local.elementTable.setColumnWidth(2, 70)
        self.ui_local.elementTable.setHorizontalHeaderItem(3, QtGui.QTableWidgetItem("Message"))
        self.ui_local.elementTable.setColumnWidth(3, 220)
        self.ui_local.elementTable.setHorizontalHeaderItem(4, QtGui.QTableWidgetItem("Msg Hz"))
        self.ui_local.elementTable.setColumnWidth(4, 50)
        self.ui_local.elementTable.setHorizontalHeaderItem(5, QtGui.QTableWidgetItem("Act Hz"))
        self.ui_local.elementTable.setColumnWidth(5, 50)
        self.ui_local.elementTable.setHorizontalHeaderItem(6, QtGui.QTableWidgetItem("PID"))
        self.ui_local.elementTable.setColumnWidth(6, 50)
        self.ui_local.elementTable.setHorizontalHeaderItem(7, QtGui.QTableWidgetItem("kB"))
        self.ui_local.elementTable.setColumnWidth(7, 50)
        self.ui_local.elementTable.setRowCount(1);
	rospy.init_node('watchdogManager')
        self.level = 0
	self.command_pub = rospy.Publisher('/watchdogCommand', WatchdogCommand)
        self.status_sub  = rospy.Subscriber('/watchdogStatus',HealthStatus,self.healthStatus)
    def killNodeButton(self):
        killItems = self.ui_local.elementTable.selectedItems()
        for x in killItems:
            rowIndex = self.ui_local.elementTable.row(x)
            nodeName = self.ui_local.elementTable.item(rowIndex,2).text()
            print(nodeName)
            return_code = call(["rosnode","kill",nodeName])

    def enableWatchdogButton(self):
        if self.level == 0:
            QtGui.QMessageBox.critical(self.ui_local.killNodeButton,"Watchdog Manager","Select a level")
        else:
            self.command_pub.publish(level=self.level,command=WatchdogCommand.ARM)
    def killWatchdogButton(self):
        if self.level == 0:
            QtGui.QMessageBox.critical(self.ui_local.killNodeButton,"Watchdog Manager","Select a level")
        else:
            self.command_pub.publish(level=self.level,command=WatchdogCommand.DISARM)
    def levelChanged(self,l):
        self.level = l
    def statusToTxt(self,l):
        if(l == HealthStatus.INIT):
            return "INIT"
        elif(l == HealthStatus.ALIVE):
            return "ALIVE"
        elif(l == HealthStatus.WARN):
            return "WARN"
        else:
            return "FAULT"
    
    
    def healthStatus(self,msg):
        if self.level == msg.level:
            txt = self.statusToTxt(msg.overallStatus) + " " + str(msg.header.seq) +"\n"+ msg.msg
            self.ui_local.overallStatus.setText(txt)
        elif self.level == 0:
            self.ui_local.overallStatus.setText("Change level to show overall status")
        if self.level == msg.level:
            # Parse and update based on this message
            self.ui_local.elementTable.setColumnCount(8);
            self.ui_local.elementTable.setRowCount(len(msg.processes));
            i = 0
            for x in msg.processes:   
                self.ui_local.elementTable.setItem(i,0,QtGui.QTableWidgetItem(str(x.element.level)))
                self.ui_local.elementTable.setItem(i,1,QtGui.QTableWidgetItem(str(self.statusToTxt(x.element.status))))
                self.ui_local.elementTable.setItem(i,2,QtGui.QTableWidgetItem(str(x.element.name)))
                self.ui_local.elementTable.setItem(i,3,QtGui.QTableWidgetItem(str(x.msg)))
                self.ui_local.elementTable.setItem(i,4,QtGui.QTableWidgetItem(str("{0:.2f}".format(x.updateFrequency))))
                self.ui_local.elementTable.setItem(i,5,QtGui.QTableWidgetItem(str("{0:.2f}".format(x.element.actualFrequency_hz))))
                self.ui_local.elementTable.setItem(i,6,QtGui.QTableWidgetItem(str(x.element.pid)))     
                kbm = x.processStatistics.rss/1024.0
                self.ui_local.elementTable.setItem(i,7,QtGui.QTableWidgetItem(str("{0:.2f}".format(kbm))))             

                i = i+1
            

if __name__ == '__main__':
    try:
	app = QtGui.QApplication(sys.argv)
	window = QtGui.QMainWindow()
	ui = Ui_MainWindow()
	ui.setupUi(window)
	ros = rosConnector(ui)
	QtCore.QObject.connect(ui.killNodeButton,QtCore.SIGNAL("clicked()"), ros.killNodeButton)
	QtCore.QObject.connect(ui.enableWatchdogButton,QtCore.SIGNAL("clicked()"), ros.enableWatchdogButton)
	QtCore.QObject.connect(ui.disableWatchdogButton,QtCore.SIGNAL("clicked()"), ros.killWatchdogButton)
        QtCore.QObject.connect(ui.levelCombo,QtCore.SIGNAL("currentIndexChanged(int)"), ros.levelChanged)
	window.show()
	app.processEvents()
	signal.signal(signal.SIGINT, signal.SIG_DFL)
    	sys.exit(app.exec_())
    except rospy.ROSInterruptException: pass
