/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/
#include <riverine_watchdog/sensors.h>


using namespace CA;

WatchdogSensor::WatchdogSensor(ros::node_handle &nh):
  n(nh)
{}

riverine_watchdog::WatchedElement updateStatus();
