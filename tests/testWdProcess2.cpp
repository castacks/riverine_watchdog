/*
* Copyright (c) 2016 Carnegie Mellon University, Author <basti@andrew.cmu.edu>
*
* For License information please see the LICENSE file in the root directory.
*
*/

#include <ros/ros.h>

#include <riverine_watchdog/watchdog.h>


int main(int argc, char **argv)
{
  ros::init(argc, argv, "testWdProcess2");
  ros::NodeHandle n("~");
  ros::Rate loop(5.0);
  CA::PetWatchdog pet;
  if(!pet.setup(n))
    {
      ROS_ERROR_STREAM("Was not able to setup watchdog");
      return -1;
    }
  while(ros::ok())
    {
      pet.alive();
      loop.sleep();
    }
  return 0;
}

