#include <ros/ros.h>

#include <riverine_watchdog/watchdog.h>


int main(int argc, char **argv)
{
  ros::init(argc, argv, "testWdProcess");
  ros::NodeHandle n("~");
  ros::Rate loop(1.0);
  CA::PetWatchdog pet;
  if(!pet.setup(n))
    {
      ROS_ERROR_STREAM("Was not able to setup watchdog");
      return -1;
    }
  while(ros::ok())
    {
      pet.alive();
      loop.sleep();
      pet.warn();
      loop.sleep();
      pet.fault();
      loop.sleep();
    }
  return 0;
}

