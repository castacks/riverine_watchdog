#!/usr/bin/env python
import roslib; roslib.load_manifest('watchdog')
import rospy
import math
from watchdog.msg import WDNodeInfo


if __name__ == '__main__':
    nodeName = 'wdHeartBeatNode01'
    rospy.init_node(nodeName, log_level=rospy.DEBUG)
    r = rospy.Rate(1) # hz
    rospy.sleep(1.0)

    pub = rospy.Publisher('/' + nodeName + '/wdHeartBeat', WDNodeInfo)

    wdHeartBeatMsg = WDNodeInfo()
    wdHeartBeatMsg.name = nodeName

    while not rospy.is_shutdown():
        now = rospy.get_time()
        
        res = int(now % 10)
        if (res == 0):
           wdHeartBeatMsg.status = WDNodeInfo.OK
           wdHeartBeatMsg.description = "Node is OK!"
        elif (res == 7):
           wdHeartBeatMsg.status = WDNodeInfo.ERROR
           wdHeartBeatMsg.description = "Node has a simulated error."

        pub.publish(wdHeartBeatMsg)

        r.sleep()

